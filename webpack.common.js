const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const createEntryPoints = require('./scripts/createEntryPoints');

const BUILD_PATH = path.join(__dirname, 'dist');

module.exports = {
	entry: {
		...createEntryPoints,
		'css/base.css': './src/css/base.css',
	},
	target: 'node',
	output: {
		path: BUILD_PATH,
		publicPath: BUILD_PATH,
		libraryTarget: 'umd',
		globalObject: 'this',
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: ['babel-loader'],
			},
			{
				test: /\.css$/,
				include: [path.resolve(__dirname, 'src/components')],
				use: ['style-loader', 'css-loader'],
			},
			{
				test: /base\.css$/,
				include: [path.resolve(__dirname, 'src/css')],
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader',
				],
			},
		],
	},
	resolve: {
		extensions: ['*', '.js', '.jsx'],
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: '[name]',
		}),
	],
};
