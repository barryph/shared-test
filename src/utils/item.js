/**
 * Convert value to user friendly string value to be displayed in UI
 * Currently converted types: Boolean
 *
 * @param {*} value
 * @returns {string}
 */

import moment from 'moment';

/**
 * Convert value from raw value to a user friendly readable format
 * for display on page. Mostly used on tables.
 *
 * @param {string} value
 * @returns {string}
 */
function convertToUserFriendlyString(value) {
	if (value === undefined || value === null) {
		return value;
	}

	if (typeof value === 'boolean') {
		return value ? 'True' : 'False';
	}

	const date = moment(value, moment.ISO_8601, true);

	if (date.isValid()) {
		return date.format('MMM DD, YYYY');
	}

	return value;
}

/**
 * Fetch property of object by string, supports nested properties
 *
 * @example
 * // returns 'Example name',
 * getObjectPropertyByString({ user: { name: 'Example name' } }, 'user.name')
 *
 * @param {object} object
 * @param {string} property - property path with dot seperated nested values
 * @returns {*} property value
 */
function getObjectPropertyByString(object, property) {
	const keys = property.split('.');
	return keys.reduce((prev, curr) => (prev ? prev[curr] : undefined), object);
}

/**
 * Transforms string of object properties to string of string of properties values
 *
 * @example
 * // returns 'Bobby Tables'
 * transformPropertyStringToValue({ name: { first: 'Bobby', last: 'Tables' } }, 'name.first name.last');
 *
 * @param {object} object
 * @param {string} string
 * @returns string
 */
function transformPropertyStringToValue(object, string, options = {}) {
	const keys = string.split(' ');
	const values = keys.map((key) => {
		const propertyString = getObjectPropertyByString(object, key.trim());
		return options.userFriendly ? convertToUserFriendlyString(propertyString) : propertyString;
	});

	// If there is only a single value return it without joining to preserve it's type
	return values.length === 1 ? values[0] : values.join(' ');
}

export default {
	convertToUserFriendlyString,
	getObjectPropertyByString,
	transformPropertyStringToValue,
};
