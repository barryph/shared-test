import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { ArrowDropUp, ArrowDropDown } from '@material-ui/icons';

import './style.css';
import itemLib from '../../utils/item';
import urlLib from '../../utils/url';
import Loader from '../Loader';

export const ORDER_ASCENDING = 'ASCENDING';
export const ORDER_DESCENDING = 'DESCENDING';
const orderIcons = {
	[ORDER_DESCENDING]: ArrowDropDown,
	[ORDER_ASCENDING]: ArrowDropUp,
};
// Used as the default sorting order when a new column is clicked
const defaultSortOrder = ORDER_ASCENDING;

const oppositeOrder = order => (order === ORDER_ASCENDING ? ORDER_DESCENDING : ORDER_ASCENDING);

/**
 * Compare stings case insensitively (Compare function for array sort)
 *
 * Blank values will always be greater so they appear at the bottom of the list
 * when sorting in ascending order
 *
 * @param {string} a
 * @param {string} b
 * @returns {number}
 */
function compareStringCaseInsensitively(a, b) {
	if (
		(typeof a === 'number' && typeof b === 'number')
		|| (typeof a === 'boolean' && typeof b === 'boolean')
	) {
		return a - b;
	}

	if (!b) return -1;
	if (!a) return 1;

	return a.toLowerCase().localeCompare(b.toLowerCase());
}

/**
 * Sort items by key
 *
 * @param {array.<object>} items
 * @param {string} key
 * @param {string} order
 * @returns {array.<object>}
 */
function sortItems(items, key, order) {
	return items.sort((a, b) => {
		let valueA = itemLib.transformPropertyStringToValue(a, key);
		let valueB = itemLib.transformPropertyStringToValue(b, key);

		if (order === ORDER_DESCENDING) {
			[valueA, valueB] = [valueB, valueA];
		}

		return compareStringCaseInsensitively(valueA, valueB);
	});
}

class ItemList extends React.Component {
	constructor(props) {
		super(props);

		this.handleColumnClick = this.handleColumnClick.bind(this);
		this.createHeaderColumns = this.createHeaderColumns.bind(this);
		this.createColumns = this.createColumns.bind(this);
		this.sortItems = this.sortItems.bind(this);

		let sortColumn = Object.keys(this.props.table)[0];

		if (this.props.defaultSortColumn) {
			if (!this.props.table[this.props.defaultSortColumn]) {
				throw new Error('Column selected for default sorting does not exist');
			}

			sortColumn = this.props.defaultSortColumn;
		}

		const order = this.props.defaultSortOrder;
		const orderBy = this.props.table[sortColumn];

		this.state = {
			order,
			orderBy,
			column: sortColumn,
			items: this.sortItems(this.props.items, orderBy, order),
		};
	}

	sortItems(items, orderBy, order) {
		return this.props.customColumnSort
			? this.props.customColumnSort(items, orderBy, order)
			: sortItems(items, orderBy, order);
	}

	handleColumnClick(column) {
		const key = this.props.table[column];
		const orderBy = key instanceof Object ? key.field : key;
		const order = this.state.orderBy === orderBy ? oppositeOrder(this.state.order) : defaultSortOrder;

		this.setState({
			order,
			column,
			orderBy,
			items: this.sortItems(this.props.items, orderBy, order),
		});
	}

	createHeaderColumns(columnNames) {
		return columnNames.map((column) => {
			const isActiveSortColumn = column === this.state.column;
			const orderOfColumn = isActiveSortColumn && this.state.order !== defaultSortOrder
				? oppositeOrder(defaultSortOrder)
				: defaultSortOrder;
			const OrderIcon = orderIcons[orderOfColumn];

			return (
				<td
					key={column}
					className={isActiveSortColumn ? 'active' : ''}
					onClick={this.handleColumnClick.bind(this, column)}
				>
					{column} {<OrderIcon />}
				</td>
			);
		});
	}

	createColumns(columnNames, item) {
		const linkBase = this.props.linkPrefix || window.location.pathname;
		const linkLocation = urlLib.joinPaths(linkBase, item.id);

		const columnsTds = columnNames.map((column, index) => {
			const value = this.props.table[column];
			let columnValue;

			if (value instanceof Object) {
				// Value is an object of the following shape
				// {
				//   field: 'rating',
				//   component: rating => <Stars rating={rating} />,
				// }
				const fieldValue = itemLib.transformPropertyStringToValue(item, value.field, {
					userFriendly: true,
				});

				columnValue = value.component(fieldValue);
			} else {
				columnValue = itemLib.transformPropertyStringToValue(item, value, {
					userFriendly: true,
				});
			}

			if (index === 0) {
				// Is first column
				if (this.props.compact) {
					return (
						<td key={`${item.id}-${column}`}>
							{this.props.noLink
								? columnValue
								: <NavLink className='link' to={linkLocation}>{columnValue}</NavLink>
							}
						</td>
					);
				}

				return (
					<td key={`${item.id}-${column}`}>
						<div className='primary-column-header'><strong>{columnValue}</strong></div>
						<ul className='list--vertical'>
							<li>
								<NavLink className='list__item link' to={linkLocation}>View</NavLink>
							</li>
							<li>
								<NavLink className='list__item link' to={`${linkLocation}/edit`}>Edit</NavLink>
							</li>
						</ul>
					</td>
				);
			}

			return <td key={`${item.id}-${column}`}>{columnValue}</td>;
		});

		if (this.props.compact && !this.props.simple) {
			columnsTds.push((
				<td key={`${item.id}-edit`}>
					<NavLink className='link' to={`${linkLocation}/edit`}>Edit</NavLink>
				</td>
			));
		}

		return columnsTds;
	}

	render() {
		const columnNames = Object.keys(this.props.table);
		const areItemsLoading = this.props.loading || !this.props.items;

		return (
			<table>
				<thead>
					<tr>{this.createHeaderColumns(columnNames)}</tr>
				</thead>
				<tbody>
					{areItemsLoading ? (
						<tr>
							<td colSpan='100'>
								<Loader />
							</td>
						</tr>
					) : (
						this.state.items.map(item => (
							<tr key={`${item.id}-${item.program_name}`}>
								{this.createColumns(columnNames, item)}
							</tr>
						))
					)}
				</tbody>
			</table>
		);
	}
}

ItemList.propTypes = {
	// Data
	items: PropTypes.arrayOf(PropTypes.object).isRequired,
	// Description of table rows to be displayed
	// Object shape: {
	//   'Displayed Column Name': 'propertyOnDataObjects'
	// }
	// Example:
	// {
	//   'Email': 'email',
	//   'Name': 'nameDisplay',
	//   'Pro': 'pro.name',
	//   'Branch': 'branch.name',
	//   'Rating': {
	//     field: 'rating',
	//     component: rating => <Stars rating={rating} />,
	//   },
	//   'Created': 'created',
	// }
	table: PropTypes.object.isRequired,

	// Modes
	// Don't display item view and edit links in first column
	compact: PropTypes.bool,
	// Don't display item edit link as final column
	simple: PropTypes.bool,
	// Should first column be a view link in (requries compact mode)
	noLink: PropTypes.bool,

	// Defaults
	defaultSortColumn: PropTypes.string,
	// Used for the initial ordering
	// Subsequent ordering of columns will use the defaultSortOrder defined at top of file
	defaultSortOrder: PropTypes.oneOf([ORDER_ASCENDING, ORDER_DESCENDING]),
	// On sort function is passed the arguments: { items, orderBy, order
	// Expected to return the sorted array of items
	customColumnSort: PropTypes.func,

	// Set a link base other than the default window.location.pathname
	// Used on dashboard pages
	linkBase: PropTypes.string,
};

ItemList.defaultProps = {
	compact: false,
	simple: false,
	noLink: false,
	defaultSortColumn: null,
	defaultSortOrder,
	customColumnSort: null,
	linkBase: null,
};

export default ItemList;
