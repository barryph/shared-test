/**
 * Form component
 *
 * Handles the boilerplate of many forms:
 * - Common UI elements - Loading state, success and error message
 * - Tracks form field values via the React Context API
 *
 * @example
 * function submitHandler(formContext) {
 *   const formData = formContext.values;
 * }
 * <Form
 *   onSubmit={submitHandler}
 *   buttonText='Submit My Form'
 *   buttonLoadingText='Submitting Form...'
 *   successMessage: 'Submission Successful!
 * >
 *   <Input
 *     name='firstName'
 *     label='First Name'
 *   />
 * </Form>
 *
 * @module form
 */

import React from 'react';
import PropTypes from 'prop-types';

import { ErrorMessage, SuccessMessage } from './Elements';

export * from './Elements';
export const FormContext = React.createContext();

export class Form extends React.Component {
	constructor(props) {
		super(props);

		// Used to scroll to a success or error message after form submission
		this.formRef = React.createRef();

		this.handleSubmit = this.handleSubmit.bind(this);

		this.state = {
			submitted: false,
			loading: false,
			errorMessage: '',
			context: {
				values: {},
			},
		};
	}

	/**
	 * Handle form submission
	 *
	 * Process:
	 *   Validate fields against Joi schema (optional)
	 *   Execute the onSubmit prop
	 *   Handle response of onSubmit
	 *
	 * @param {object} event
	 * @param {object} formContext
	 */
	async handleSubmit(event, formContext) {
		event.preventDefault();

		this.setState({
			loading: true,
		});

		const response = await this.props.onSubmit(formContext) || {};

		if (response.redirecting) {
			// Stop execution, new page will load and this form will be unmounted
			return;
		}

		const stateUpdates = {
			submitted: true,
			loading: false,
			errorMessage: null,
		};

		if (response.error) {
			// Optionally an error message returned in response could be used
			if (typeof response.error === 'string') {
				stateUpdates.errorMessage = response.error;
			} else {
				stateUpdates.errorMessage = 'Something went wrong while trying to submit your form. Please try again.';
			}
		}

		this.setState(stateUpdates);

		window.scrollTo({
			top: this.formRef.current.offsetTop - 200,
			behavior: 'smooth',
		});
	}

	render() {
		return (
			<FormContext.Provider
				value={this.state.context}
			>
				<FormContext.Consumer>
					{form => (
						<form ref={this.formRef} onSubmit={event => this.handleSubmit(event, form)}>
							{this.state.submitted && (
								<div className='block--md'>
									{
										this.state.errorMessage
											? <ErrorMessage>{this.state.errorMessage}</ErrorMessage>
											: <SuccessMessage>{this.props.successMessage}</SuccessMessage>
									}
								</div>
							)}

							{this.props.children}

							<button className={`button button--lg ${this.props.buttonFull ? 'button--full' : ''}`}>
								{this.state.loading ? this.props.buttonLoadingText : this.props.buttonText}</button>
						</form>
					)}
				</FormContext.Consumer>
			</FormContext.Provider>
		);
	}
}

Form.propTypes = {
	onSubmit: PropTypes.func.isRequired,
	buttonText: PropTypes.string.isRequired,
	buttonLoadingText: PropTypes.string.isRequired,
	buttonFull: PropTypes.bool,
	successMessage: PropTypes.string,
};

Form.defaultProps = {
	buttonFull: false,
	successMessage: 'Your submission was successful!',
};

export default Form;
