import React from 'react';
import PropTypes from 'prop-types';

import './style.css';
import itemLib from '../../utils/item';
import PanelLabeled from '../PanelLabeled';


function getColumnChildren(value, item) {
	if (React.isValidElement(value)) {
		return value;
	}

	return itemLib.transformPropertyStringToValue(item, value, {
		userFriendly: true,
	});
}

function getPanelChildren(tableMap, item) {
	// If value is a react element render it
	if (React.isValidElement(tableMap)) {
		return tableMap;
	}

	return (
		<table className='table-list table-list--vertical'>
			<tbody>
				{Object.keys(tableMap).map((column) => {
					const children = getColumnChildren(tableMap[column], item);

					// Ignore columns with undefined or falsey values
					if (!children) return null;

					return (
						<tr className='table-list__item' key={`${column}`}>
							<th>{column}</th>
							<td>{children}</td>
						</tr>
					);
				})}
			</tbody>
		</table>
	);
}

const Split = (props) => {
	let tableHeadline = '';

	return props.data.map((value, i) => {
		// If value is string (instead of object) parse it as a panel headline
		if (typeof value === 'string') {
			tableHeadline = value;
			return null;
		}

		const children = getPanelChildren(value, props.item);

		return (
			<PanelLabeled label={tableHeadline} key={`${tableHeadline}-${i}`}>
				{children}
			</PanelLabeled>
		);
	});
};


class ItemView extends React.Component {
	render() {
		const { item, tables } = this.props;
		const split1Data = tables[0] || [];
		const split2Data = tables[1] || [];

		return (
			<div className='split'>
				<div className='split__item'>
					<Split item={item} data={split1Data} />
				</div>
				<div className='split__item'>
					<Split item={item} data={split2Data} />
				</div>
			</div>
		);
	}
}

ItemView.propTypes = {
	// Object where all string values of the table are sourced from
	item: PropTypes.object.isRequired,
	// Description of table rows to be displayed
	// Example:
	// [[
	//   'Information', {
	//     'ID': 'id',
	//     'Name': 'username',
	//     'Pro': 'pro.name',
	//     'Rating': <Stars rating={review.rating} />,
	//     'Created': 'created',
	//     'Updated': 'updated',
	//   },
	// ], []]
	tables: PropTypes.array.isRequired,
};

export default ItemView;
