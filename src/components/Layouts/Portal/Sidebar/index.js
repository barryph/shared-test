import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import './style.css';

const Sidebar = props => (
	<aside className={`site-sidebar ${props.isOpen ? '' : 'closed'}`}>
		<NavLink exact to='/' className='logo-wrapper'>
			<img className='logo' src={props.logo} alt='Rachio logo'/>
		</NavLink>

		<div className='nav-header'>Sections</div>
		<nav>
			<NavLink to='/admin'>Admin</NavLink>
		</nav>
	</aside>
);

Sidebar.propTypes = {
	isOpen: PropTypes.bool.isRequired,
	logo: PropTypes.string.isRequired,
};

export default Sidebar;
