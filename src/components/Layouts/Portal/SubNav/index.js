import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import './style.css';

const SubNav = props => (
	<div className='sub-nav__container'>
		<nav className='sub-nav passive container--full'>
			<NavLink to='/admin/pros'>Pros</NavLink>
			<NavLink to='/admin/branches'>Branches</NavLink>
			<NavLink to='/admin/leads'>Leads</NavLink>
			<NavLink to='/admin/reviews'>Reviews</NavLink>

			{props.children}
		</nav>
	</div>
);

SubNav.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.element,
		PropTypes.array,
	]),
};

SubNav.defaultProps = {
	children: null,
};

export default SubNav;
