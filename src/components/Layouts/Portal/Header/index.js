import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

class Header extends React.Component {
	render() {
		return (
			<nav className='site-header passive container--full'>
				<span>
					<a className='link--passive icon icon--lg nav-trigger' onClick={this.props.onSidebarToggle}>&#9776;</a>
					<span className='icon__text'>Admin</span>
				</span>

				<span>
					<a onClick={this.props.onLogout} key={'signout-link'}>Sign Out</a>
				</span>
			</nav>
		);
	}
}

Header.propTypes = {
	onSidebarToggle: PropTypes.func.isRequired,
	onLogout: PropTypes.func.isRequired,
};

export default Header;
