import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

import Sidebar from './Sidebar';
import Header from './Header';
import SubNav from './SubNav';

class PortalLayout extends React.Component {
	constructor(props) {
		super(props);

		this.toggleSidebar = this.toggleSidebar.bind(this);

		this.state = {
			isUserAuthed: false,
			isSidebarOpen: true,
		};
	}

	toggleSidebar() {
		this.setState({
			isSidebarOpen: !this.state.isSidebarOpen,
		});
	}

	async componentDidMount() {
		// Authentication info is stored in localStorage
		const auth = localStorage.getItem('auth');

		if (!auth) {
			this.props.onUnauthorized();
			return;
		}

		this.props.onAuthorized(auth);

		this.setState({
			isUserAuthed: true,
			isSidebarOpen: window.innerWidth > 600,
		});
	}

	render() {
		// Stop flashing of screen before redirecting onUnauthorized call
		if (!this.state.isUserAuthed) return null;

		const { component: Component } = this.props;

		return (
			<div className='site-root'>
				<Helmet>
					{/* Title can be overriden on each page using react-helmet */}
					<title>Portal | Rachio Pro</title>
				</Helmet>

				<Sidebar isOpen={this.state.isSidebarOpen} logo={this.props.logo} />
				<main>
					<Header onSidebarToggle={this.toggleSidebar} onLogout={this.props.onLogout} />
					<SubNav />

					<Component {...this.props.matchProps} />
				</main>
			</div>
		);
	}
}

PortalLayout.propTypes = {
	logo: PropTypes.string.isRequired,
	// Generally redirect user to login page
	onUnauthorized: PropTypes.func.isRequired,
	// Generally set auth in (redux) auth store
	onAuthorized: PropTypes.func.isRequired,
	onLogout: PropTypes.func.isRequired,
	// Page react component
	component: PropTypes.func.isRequired,
	// React router match props
	matchProps: PropTypes.object.isRequired,
};

export default PortalLayout;
